//
//  Data.swift
//  Zoo
//
//  Created by Viral Doshi on 30/01/23.
//

import Foundation

struct Details{
    let title: String;
    let descr: String;
}
