//
//  ViewController.swift
//  Zoo
//
//  Created by Viral Doshi on 30/01/23.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    var details = [
        Details(title: "Dog", descr: "I am a Dog"),
        Details(title: "Cat", descr: "I am a Cat"),
        Details(title: "Monkey", descr: "I am a Monkey"),
        Details(title: "Lion", descr: "I am a Lion"),
        Details(title: "Tiger", descr: "I am a Tiger"),
        Details(title: "Zebra", descr: "I am a Zebra"),
        Details(title: "Peacock", descr: "I am a Peacock"),
        Details(title: "Squirrel", descr: "I am a Squirrel"),
        
    ]

    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        TableView.delegate = self
        TableView.dataSource = self
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return details.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : TableViewCell = TableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.TitleCell.text = details[indexPath.row].title
        
        cell.DescriptionCell.text = details[indexPath.row].descr
        
        cell.ImageView.image = UIImage(named: details[indexPath.row].title)
        return cell
    }
    

}

