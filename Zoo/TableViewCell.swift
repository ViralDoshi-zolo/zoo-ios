//
//  TableViewCell.swift
//  Zoo
//
//  Created by Viral Doshi on 30/01/23.
//

import UIKit

class TableViewCell: UITableViewCell {

   
    @IBOutlet weak var ImageView: UIImageView!
    
    @IBOutlet weak var DescriptionCell: UILabel!
    
    
    @IBOutlet weak var TitleCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
